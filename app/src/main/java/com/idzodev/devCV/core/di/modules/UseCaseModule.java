package com.idzodev.devCV.core.di.modules;

import com.idzodev.devCV.core.executors.PostExecutionThread;
import com.idzodev.devCV.core.executors.ThreadExecutor;
import com.idzodev.devCV.domain.repository.EntitiesRepository;
import com.idzodev.devCV.domain.repository.PreferenceRepository;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.domain.use_case.impl.UserUseCaseImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by vladimir on 02.06.16.
 */
@Module(includes = {
        RepositoriesModule.class,
        ThreadExecutorsModule.class,
//        ApiModule.class
})
public class UseCaseModule {

    @Provides
    @Singleton
    UserUseCase provideUserUserCase(Retrofit retrofit, PreferenceRepository preferenceRepository, EntitiesRepository entitiesRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread){
        return new UserUseCaseImpl(preferenceRepository,retrofit,entitiesRepository, postExecutionThread, threadExecutor);
    }
}
