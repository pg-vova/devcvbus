package com.idzodev.devCV.core.di.modules;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.domain.entities.DaoMaster;
import com.idzodev.devCV.domain.entities.DaoSession;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vladimir on 12.08.16.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    DaoSession provideDaoSession(App app){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(app, "pokemon-test", null);
        DaoMaster master = new DaoMaster(helper.getWritableDatabase());
        return master.newSession();
    }
}
