package com.idzodev.devCV.core.di.modules;


import com.idzodev.devCV.core.executors.PostExecutionThread;
import com.idzodev.devCV.core.executors.ThreadExecutor;
import com.idzodev.devCV.domain.executors.JobExecutor;
import com.idzodev.devCV.domain.executors.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vladimir on 02.06.16.
 */
@Module
public class ThreadExecutorsModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecuter(){
        return new JobExecutor();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread (){
        return new UIThread();
    }


}
