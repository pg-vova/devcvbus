package com.idzodev.devCV.core.di.modules;


import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.domain.entities.DaoSession;
import com.idzodev.devCV.domain.repository.EntitiesRepository;
import com.idzodev.devCV.domain.repository.PreferenceRepository;
import com.idzodev.devCV.domain.repository.impl.EntitiesRepositoryImpl;
import com.idzodev.devCV.domain.repository.impl.PreferenceRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
/**
 * Created by vladimir on 02.06.16.
 */
@Module(includes = {
        DataModule.class,
        MappersModule.class
})
public class RepositoriesModule {

    @Provides
    @Singleton
    EntitiesRepository provideEntitiesRepository(DaoSession daoSession){
        return new EntitiesRepositoryImpl(daoSession);
    }

    @Provides
    @Singleton
    PreferenceRepository providePreferenceRepository(App app){
        return new PreferenceRepositoryImpl(app);
    }
}
