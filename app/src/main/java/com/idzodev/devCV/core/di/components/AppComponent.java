package com.idzodev.devCV.core.di.components;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.bus.Bus;
import com.idzodev.devCV.core.di.modules.ApiModule;
import com.idzodev.devCV.core.di.modules.AppModule;
import com.idzodev.devCV.core.di.modules.DataModule;
import com.idzodev.devCV.core.di.modules.MappersModule;
import com.idzodev.devCV.core.di.modules.RepositoriesModule;
import com.idzodev.devCV.core.di.modules.ThreadExecutorsModule;
import com.idzodev.devCV.core.di.modules.UseCaseModule;
import com.idzodev.devCV.core.executors.PostExecutionThread;
import com.idzodev.devCV.core.executors.ThreadExecutor;
import com.idzodev.devCV.domain.repository.PreferenceRepository;
import com.idzodev.devCV.services.location.FusedLocationProvider;
import com.idzodev.devCV.ui.di.HomeComponent;
import com.idzodev.devCV.ui.di.LoginComponent;
import com.idzodev.devCV.ui.di.MapComponent;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by vladimir on 25.07.16.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class,
        UseCaseModule.class,
        RepositoriesModule.class,
        MappersModule.class
})
public interface AppComponent {
    ServicesComponent plus(ServicesComponent.ServicesModule module);
    MapComponent plus(MapComponent.Module module);
    HomeComponent plus(HomeComponent.Module module);
    LoginComponent plus(LoginComponent.Module module);

    App getApp();
    Bus getBus();
    PreferenceRepository getPreferenceRepository();
    FusedLocationProvider getFusedLocationProvider();
    ThreadExecutor getJobExcecutor();
    PostExecutionThread getUIThread();
}
