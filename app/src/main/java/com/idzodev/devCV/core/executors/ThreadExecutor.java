package com.idzodev.devCV.core.executors;

import java.util.concurrent.Executor;

/**
 * Created by vladimir on 02.06.16.
 */
public interface ThreadExecutor extends Executor {}
