package com.idzodev.devCV.domain.api;



import com.idzodev.devCV.domain.api.response.LoginResponse;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by on 13.12.16.
 */

public interface UserApi {

    @GET("accaunt/login")
    Observable<LoginResponse> loginUser(@Query(value = "mail") String mail, @Query(value = "password") String password );

    @POST("bus/postbusposition")
    Observable<Object> postPosition(@Query(value = "userid") Long userid,@Query(value = "key") String key, @Query(value = "latitude") double latitude,@Query(value = "longitude") double longitude);
}
