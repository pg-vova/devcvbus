package com.idzodev.devCV.domain.use_case.impl;

import com.idzodev.devCV.core.executors.PostExecutionThread;
import com.idzodev.devCV.core.executors.ThreadExecutor;
import com.idzodev.devCV.domain.api.UserApi;
import com.idzodev.devCV.domain.entities.MainEntity;
import com.idzodev.devCV.domain.repository.EntitiesRepository;
import com.idzodev.devCV.domain.repository.PreferenceRepository;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.ui.dvo.DriverDvo;
import com.idzodev.devCV.ui.dvo.EntityDvo;
import com.idzodev.devCV.ui.dvo.MapDataDvo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by vladimir on 12.08.16.
 */
public class UserUseCaseImpl implements UserUseCase {

    private PreferenceRepository preferenceRepository;
    private EntitiesRepository entitiesRepository;
    private PostExecutionThread postExecutionThread;
    private ThreadExecutor threadExecutor;
    private UserApi userApi;

    @Inject
    public UserUseCaseImpl(PreferenceRepository preferenceRepository,Retrofit retrofit, EntitiesRepository entitiesRepository, PostExecutionThread postExecutionThread, ThreadExecutor threadExecutor) {
        this.preferenceRepository = preferenceRepository;
        this.userApi = retrofit.create(UserApi.class);
        this.entitiesRepository = entitiesRepository;
        this.postExecutionThread = postExecutionThread;
        this.threadExecutor = threadExecutor;
    }

    @Override
    public Observable<String> postUserPos( double lat, double lan) {
        return userApi.postPosition(preferenceRepository.getId(),preferenceRepository.getKey(),lat,lan)
                .doOnNext(o -> {

                })
                .map(o -> "")
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    @Override
    public Observable<String> login(String login, String password) {
        return userApi.loginUser(login,password)
                .doOnNext(o -> {
                    preferenceRepository.saveUserId(o.getUserId());
                    preferenceRepository.saveUserKey(o.getKey());
                })
                .map(o -> "")
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }


}
