package com.idzodev.devCV.domain.repository.impl;

import android.content.Context;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.android.BasePreference;
import com.idzodev.devCV.domain.repository.PreferenceRepository;

import javax.inject.Inject;

/**
 * Created by vladimir on 12.08.16.
 */
public class PreferenceRepositoryImpl extends BasePreference implements PreferenceRepository {
    private static final String IS_FIRST_START = "IS_FIRST_START";
    private static final String PREF_KEY = "PREF_KEY";
    private static final String PREF_ID = "PREF_ID";

    private App app;

    @Inject
    public PreferenceRepositoryImpl(App app) {
        this.app = app;
    }

    @Override
    protected Context getContext() {
        return app;
    }

    @Override
    public void saveUserKey(String key) {
        save(PREF_KEY,key);
    }

    @Override
    public void saveUserId(long id) {
        save(PREF_ID,id);
    }

    @Override
    public String getKey() {
        return getStr(PREF_KEY);
    }

    @Override
    public long getId() {
        return getLong(PREF_ID);
    }

    @Override
    public boolean isFirstStart() {
        return getLong(IS_FIRST_START) == 1;
    }

    @Override
    public void setFirstStart(boolean isFirstStart) {
        save(IS_FIRST_START, isFirstStart ? 1l : 0l);
    }
}
