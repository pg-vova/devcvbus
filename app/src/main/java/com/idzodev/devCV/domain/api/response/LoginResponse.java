package com.idzodev.devCV.domain.api.response;

/**
 * Created by on 18.09.17.
 */

public class LoginResponse {

    int UserId;
    String Key;
    String Bus;

    public LoginResponse() {
    }

    public LoginResponse(int userId, String key, String bus) {
        UserId = userId;
        Key = key;
        Bus = bus;
    }

    public int getUserId() {
        return UserId;
    }

    public String getKey() {
        return Key;
    }

    public String getBus() {
        return Bus;
    }
}
