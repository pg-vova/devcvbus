package com.idzodev.devCV.domain.use_case;

import com.idzodev.devCV.ui.dvo.DriverDvo;
import com.idzodev.devCV.ui.dvo.MapDataDvo;

import rx.Observable;

/**
 * Created by vladimir on 12.08.16.
 */
public interface UserUseCase {
    Observable<String> postUserPos( double lat, double lon);
    Observable<String> login(String login,String password);
}
