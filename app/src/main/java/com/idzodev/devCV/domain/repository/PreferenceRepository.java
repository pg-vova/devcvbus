package com.idzodev.devCV.domain.repository;

/**
 * Created by vladimir on 12.08.16.
 */
public interface PreferenceRepository {

    void saveUserKey(String key);
    void saveUserId(long id);
    String getKey();
    long getId();

    boolean isFirstStart();
    void setFirstStart(boolean isFirstStart);
}
