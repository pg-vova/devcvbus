package com.idzodev.devCV.ui.di;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.di.scope.PerActivity;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.ui.screens.login.LoginActivity;
import com.idzodev.devCV.ui.screens.login.LoginPresenter;
import com.idzodev.devCV.ui.screens.login.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by on 18.09.17.
 */
@Subcomponent(modules = LoginComponent.Module.class)
@PerActivity
public interface LoginComponent {

    void inject(LoginActivity activity);

    @dagger.Module
    class Module{

        @Provides
        @PerActivity
        LoginPresenter provideLoginPresenter(App app, UserUseCase userUseCase){
            return new LoginPresenterImpl(app, userUseCase);
        }
    }
}
