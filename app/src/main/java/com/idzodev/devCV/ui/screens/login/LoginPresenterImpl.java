package com.idzodev.devCV.ui.screens.login;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.mvp.BasePresenter;
import com.idzodev.devCV.core.rx.SimpleSubscriber;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.ui.dvo.DriverDvo;

/**
 * Created by on 18.09.17.
 */

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    App app;
    UserUseCase userUseCase;

    public LoginPresenterImpl(App app, UserUseCase userUseCase) {
        this.app = app;
        this.userUseCase = userUseCase;
    }

    @Override
    protected void onViewAttached() {
        super.onViewAttached();
    }

    @Override
    public void onLoginClick(String email, String password) {
        addSubscription(userUseCase.login(email,password).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                getView().openHomeScreen();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
            }
        }));
    }
}
