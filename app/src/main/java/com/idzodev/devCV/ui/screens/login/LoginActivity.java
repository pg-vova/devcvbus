package com.idzodev.devCV.ui.screens.login;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.EditText;

import com.idzodev.devCV.R;
import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.android.BaseActivity;
import com.idzodev.devCV.core.rx.SimpleSubscriber;
import com.idzodev.devCV.core.rx.TextWatcherObservable;
import com.idzodev.devCV.core.utils.EmailValidator;
import com.idzodev.devCV.core.utils.StringUtils;
import com.idzodev.devCV.ui.di.LoginComponent;
import com.idzodev.devCV.ui.screens.home.HomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity implements LoginView{

    @BindView(R.id.email)
    EditText vEmail;
    @BindView(R.id.password)
    EditText vPass;


    @Inject
    LoginPresenter mLoginPresenter;

    CompositeSubscription subscription = new CompositeSubscription();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setupDagger();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mLoginPresenter.attachView(this);
        subscription.add(TextWatcherObservable.create(vEmail).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                validate();
            }
        }));

        subscription.add(TextWatcherObservable.create(vPass).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                validate();
            }
        }));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mLoginPresenter.detachView();
    }

    private void setupDagger(){
        App.getApp(this)
                .getAppComponent()
                .plus(new LoginComponent.Module())
                .inject(this);
    }

    public boolean validate() {
        String email = vEmail.getText().toString();
        String password = vPass.getText().toString();
        boolean isAllFine = true;
        if (!EmailValidator.isValid(email)) {
            isAllFine = false;
            vEmail.setTextColor(ContextCompat.getColor(this, R.color.colorTextError));
        } else {
            vEmail.setTextColor(ContextCompat.getColor(this, R.color.colorText));
        }
        if (StringUtils.isNullEmpty(password)) {
            isAllFine = false;
        }
        if (StringUtils.isNullEmpty(email)) {
            isAllFine = false;
        }
        if (password.length() < 2) {
            isAllFine = false;
            vPass.setTextColor(ContextCompat.getColor(this, R.color.colorTextError));
        } else {
            vPass.setTextColor(ContextCompat.getColor(this, R.color.colorText));
        }
        return isAllFine;
    }


    @OnClick(R.id.email_sign_in_button)
    public void login(){
        String email = vEmail.getText().toString();
        String password = vPass.getText().toString();
        if (validate())
        mLoginPresenter.onLoginClick(email,password);
    }

    @Override
    public void openHomeScreen() {
        HomeActivity.start(this);
    }
}

