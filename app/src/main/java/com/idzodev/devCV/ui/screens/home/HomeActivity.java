package com.idzodev.devCV.ui.screens.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.idzodev.devCV.R;
import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.android.BaseActivity;
import com.idzodev.devCV.core.rx.SimpleSubscriber;
import com.idzodev.devCV.core.rx.TextWatcherObservable;
import com.idzodev.devCV.core.utils.StringUtils;
import com.idzodev.devCV.domain.repository.PreferenceRepository;
import com.idzodev.devCV.ui.di.HomeComponent;
import com.idzodev.devCV.ui.screens.GetMyLocationActivity;
import com.idzodev.devCV.ui.screens.map.MapActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by vladimir on 12.08.16.
 */
public class HomeActivity extends BaseActivity implements HomeView{
    private static final int REQUEST_MY_LOCATION = 1;

    @BindView(R.id.home_activity_mail)
    EditText vEmail;
    @BindView(R.id.home_activity_password)
    EditText vPass;
    @BindView(R.id.home_activity_btn_login)
    TextView vLogin;

    @Inject
    protected HomePresenter homePresenter;

    CompositeSubscription subscription = new CompositeSubscription();
    @Inject
    PreferenceRepository preferenceRepository;

    public static void start(Activity activity){
        activity.startActivity(new Intent(activity, HomeActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);
        preferenceRepository = App.getApp(this)
                .getAppComponent()
                .getPreferenceRepository();
        App.getApp(this)
                .getAppComponent()
                .plus(new HomeComponent.Module())
                .inject(this);
        ButterKnife.bind(this);
        homePresenter.attachView(this);
        if (!StringUtils.isNullEmpty(preferenceRepository.getKey())){
            MapActivity.start(this);
            finish();
        }
        vLogin.setEnabled(false);
        GetMyLocationActivity.start(this, REQUEST_MY_LOCATION);

    }

    @Override
    protected void onStart() {
        super.onStart();

        subscription.add(TextWatcherObservable.create(vEmail)
                .subscribe(new SimpleSubscriber<String>() {
                    @Override
                    public void onNext(String item) {
                        validate();
                    }
                }));


        subscription.add(TextWatcherObservable.create(vPass)
                .subscribe(new SimpleSubscriber<String>() {
                    @Override
                    public void onNext(String item) {
                        validate();
                    }
                }));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.detachView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_MY_LOCATION && resultCode == RESULT_OK){
            final double lat = data.getDoubleExtra(GetMyLocationActivity.EXTRA_LAT, 0);
            final double lon = data.getDoubleExtra(GetMyLocationActivity.EXTRA_LON, 0);
            homePresenter.onMyLocationSelected(lat, lon);

        }
    }

    @Override
    public void showMapScreen() {
        MapActivity.start(this);
        finish();
    }

    @Override
    public void showGetMyLocationScreen() {
        GetMyLocationActivity.start(this, REQUEST_MY_LOCATION);
    }


    public void validate() {
        boolean isAllFine = true;
        String email = vEmail.getText().toString();
        String pass = vPass.getText().toString();
        if (StringUtils.isNullEmpty(email)) {
            isAllFine = false;
        }
        if (StringUtils.isNullEmpty(pass)) {
            isAllFine = false;
        }
        if (email.length() < 2) {
            isAllFine = false;
        }
        if (pass.length() < 6) {
            isAllFine = false;
        }

        if (isAllFine) {
            enableNext(true);
        } else {
            enableNext(false);
        }
    }

    public void enableNext(boolean enable) {
        vLogin.setEnabled(enable);
    }

    @OnClick(R.id.home_activity_btn_login)
    public void onLoginClick(){
        Toast.makeText(this,vEmail.getText().toString() + "  " + vPass.getText().toString(),Toast.LENGTH_LONG).show();
        homePresenter.onLoginClick(
                vEmail.getText().toString(),
                vPass.getText().toString()
        );
    }




}
