package com.idzodev.devCV.ui.screens.map;

import com.idzodev.devCV.ui.dvo.MapDataDvo;

/**
 * Created by vladimir on 12.08.16.
 */
public interface MapView {
    void showProgress();
    void hideProgress();
    void showMyPosition(double lat, double lan);
    void showMapData(MapDataDvo mapDataDvo);
    void removeOldMapData();

    void cameraMovementDisable();
    void cameraMovementEnable();
    void moveCamera(double lat, double lon);
}
