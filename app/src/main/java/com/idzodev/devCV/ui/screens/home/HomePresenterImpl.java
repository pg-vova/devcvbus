package com.idzodev.devCV.ui.screens.home;

import com.idzodev.devCV.core.mvp.BasePresenter;
import com.idzodev.devCV.core.rx.SimpleSubscriber;
import com.idzodev.devCV.domain.entities.MainEntity;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.ui.dvo.DriverDvo;

/**
 * Created by vladimir on 15.08.16.
 */
public class HomePresenterImpl extends BasePresenter<HomeView> implements HomePresenter {
    private UserUseCase userUseCase;

    private boolean isLocationSelected = false;
    private double lat;
    private double lon;
    private int lastSelectedType = -1;

    public HomePresenterImpl(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @Override
    public void onMyLocationSelected(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
        isLocationSelected = true;

    }

    @Override
    public boolean isMyLocationSelected() {
        return isLocationSelected;
    }

    @Override
    public void onLoginClick(String login, String password) {
        if (getView() == null) return;

        if (!isMyLocationSelected()){
            getView().showGetMyLocationScreen();
            return;
        }
        addSubscription(userUseCase.login(login,password).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                getView().showGetMyLocationScreen();
                getView().showMapScreen();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
            }
        }));
    }



}
