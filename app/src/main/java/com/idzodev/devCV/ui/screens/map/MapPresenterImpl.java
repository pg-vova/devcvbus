package com.idzodev.devCV.ui.screens.map;

import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.mvp.BasePresenter;
import com.idzodev.devCV.core.rx.SimpleSubscriber;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.ui.dvo.MapDataDvo;

import javax.inject.Inject;

/**
 * Created by vladimir on 12.08.16.
 */
public class MapPresenterImpl extends BasePresenter<MapView> implements MapPresenter{

    App app;

    private UserUseCase userUseCase;
    private boolean isCameraDisable;
    private LatLng lastKnownLocation;

    @Inject
    public MapPresenterImpl(App app, UserUseCase userUseCase) {
        this.app = app;
        this.userUseCase = userUseCase;
        this.isCameraDisable = false;
    }

    @Override
    protected void onViewAttached() {
        getView().showProgress();
    }

    @Override
    public void updateData(double lat, double lon) {
        if (getView() == null) return;

        getView().hideProgress();
        getView().removeOldMapData();
        getView().showMyPosition(lat, lon);
        if (isCameraDisable || lastKnownLocation == null){
            getView().moveCamera(lat, lon);
        }
        lastKnownLocation = new LatLng(lat, lon);
    }

    @Override
    public void onDisableCameraMovement(boolean disable) {
        isCameraDisable = disable;
        if (isCameraDisable){
            getView().cameraMovementDisable();
            if (lastKnownLocation != null)
                getView().moveCamera(lastKnownLocation.latitude, lastKnownLocation.longitude);
        } else {
            getView().cameraMovementEnable();
        }
    }

    @Override
    public void onMarkerClicked(Marker markerOptions) {

    }

    @Override
    public void sendLocation(double lat, double lon) {
        if (getView() == null) return;
        addSubscription(userUseCase.postUserPos(lat, lon).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
            }
        }));
        Log.d("locationsender",lat + "  " +lon);
    }

    @Override
    public void onLocationChange(Location location) {
        if (location == null)
            return;
        updateData(location.getLatitude(), location.getLongitude());
    }

    @Override
    public void onShowSetupLocationSettingsPopup() {

    }

    @Override
    public void onResolutionRequired(Status status) {

    }
}
