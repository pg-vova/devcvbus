package com.idzodev.devCV.ui.screens.map;

import android.app.Activity;
import android.app.job.JobScheduler;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.android.BaseActivity;
import com.idzodev.devCV.core.utils.MapUtils;
import com.idzodev.devCV.core.utils.MarkerUtils;
import com.idzodev.devCV.domain.repository.EntitiesRepository;
import com.idzodev.devCV.services.location.FusedLocationProvider;
import com.idzodev.devCV.ui.di.MapComponent;
import com.idzodev.devCV.ui.dvo.EntityDvo;
import com.idzodev.devCV.ui.dvo.MapDataDvo;
import com.idzodev.devCV.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import rx.Scheduler;

/**
 * Created by vladimir on 12.08.16.
 */
public class MapActivity extends BaseActivity implements MapView, OnMapReadyCallback{

    @BindView(R.id.progress)
    protected View vProgress;

    @Inject
    protected FusedLocationProvider fusedLocationProvider;
    @Inject
    protected MapPresenter mapPresenter;
    @BindView(R.id.map_activity_settings)
    protected CheckBox checkBox;

    private GoogleMap googleMap;
    private Marker myLastLocationMarker;
    private Circle meLastLocationCircle;
    private MarkerAdapter markerAdapter;
    private List<Marker> entities = new ArrayList<>();

    //Android native
    public static void start(Activity activity){
        Intent intent = new Intent(activity, MapActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        setupDagger();
        ButterKnife.bind(this);
        SupportMapFragment fragment = SupportMapFragment.newInstance();
        fragment.getMapAsync(this);
        replaceFragment(fragment, SupportMapFragment.class.getName()).commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProvider.unregister();
        mapPresenter.detachView();
        googleMap = null;
    }

    // View
    @Override
    public void showProgress() {
        vProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        vProgress.setVisibility(View.GONE);
    }

    @Override
    public void showMyPosition(double lat, double lan) {
        if (googleMap == null) return;

        if (myLastLocationMarker != null){
            myLastLocationMarker.setPosition(new LatLng(lat, lan));
        } else {
            myLastLocationMarker = googleMap.addMarker(MarkerUtils.createMyMarker(this, 0, "Я", lat, lan));
        }


    }

    @Override
    public void showMapData(MapDataDvo mapDataDvo) {
        showMyPosition(myLastLocationMarker.getPosition().latitude, myLastLocationMarker.getPosition().longitude);
    }

    @Override
    public void removeOldMapData() {
        for (Marker marker : entities){
            marker.remove();
        }
        entities.clear();
    }

    @Override
    public void cameraMovementDisable() {
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
    }

    @Override
    public void cameraMovementEnable() {
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
    }

    @Override
    public void moveCamera(double lat, double lon) {
        MapUtils.cameraGoTo(googleMap, new LatLng(lat, lon), 15);
    }

    private void setupDagger(){
        App.getApp(this)
                .getAppComponent()
                .plus(new MapComponent.Module())
                .inject(this);
    }

    // Android listeners
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.getUiSettings().setCompassEnabled(false);
//        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setIndoorLevelPickerEnabled(false);
//        googleMap.getUiSettings().setMapToolbarEnabled(false);
//        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
//        googleMap.setOnMarkerClickListener(marker -> {
//            marker.showInfoWindow();
//            mapPresenter.onMarkerClicked(marker);
//            return true;
//        });

        markerAdapter = new MarkerAdapter(this);
        googleMap.setInfoWindowAdapter(markerAdapter);

        mapPresenter.attachView(this);
        mapPresenter.onLocationChange(fusedLocationProvider.getLastLocation(this));
        fusedLocationProvider.register(mapPresenter);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (myLastLocationMarker != null)
                mapPresenter.sendLocation(myLastLocationMarker.getPosition().latitude,myLastLocationMarker.getPosition().longitude);
                handler.postDelayed(this,10000);
            }
        },10000);
    }

    @OnCheckedChanged(R.id.map_activity_settings)
    public void onCameraCheckListener(){
        mapPresenter.onDisableCameraMovement(checkBox.isChecked());
    }

    @Override
    public void onBackPressed() {

    }
}
