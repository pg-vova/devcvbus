package com.idzodev.devCV.ui.screens.home;

import com.idzodev.devCV.core.mvp.Presenter;

/**
 * Created by vladimir on 15.08.16.
 */
public interface HomePresenter extends Presenter<HomeView> {
    void onMyLocationSelected(double lat, double lon);
    boolean isMyLocationSelected();

    void onLoginClick(String login, String  password);
}
