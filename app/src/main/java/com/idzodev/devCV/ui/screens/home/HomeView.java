package com.idzodev.devCV.ui.screens.home;

/**
 * Created by vladimir on 15.08.16.
 */
public interface HomeView {


    void showMapScreen();
    void showGetMyLocationScreen();
}
