package com.idzodev.devCV.ui.screens.login;

import com.idzodev.devCV.core.mvp.Presenter;

/**
 * Created by on 18.09.17.
 */

public interface LoginPresenter extends Presenter<LoginView> {
    void onLoginClick(String email,String password);
}
