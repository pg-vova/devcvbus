package com.idzodev.devCV.ui.di;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.di.scope.PerActivity;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.services.location.FusedLocationProvider;
import com.idzodev.devCV.ui.screens.home.HomeActivity;
import com.idzodev.devCV.ui.screens.home.HomePresenter;
import com.idzodev.devCV.ui.screens.home.HomePresenterImpl;

import javax.inject.Singleton;

import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by vladimir on 15.08.16.
 */
@Subcomponent(modules = {HomeComponent.Module.class})
@PerActivity
public interface HomeComponent {
    void inject(HomeActivity homeActivity);

    @dagger.Module
    class Module{
        @Provides
        @PerActivity
        public HomePresenter provideHomePresenter(UserUseCase userUseCase){
            return new HomePresenterImpl(userUseCase);
        }

        @Singleton
        @PerActivity
        public FusedLocationProvider providerFusedLocationProvider(App app){
            return new FusedLocationProvider(app);
        }
    }
}
