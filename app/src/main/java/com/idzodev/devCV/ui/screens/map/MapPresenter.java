package com.idzodev.devCV.ui.screens.map;

import com.google.android.gms.maps.model.Marker;
import com.idzodev.devCV.core.mvp.Presenter;
import com.idzodev.devCV.services.location.FusedLocationProvider;

/**
 * Created by vladimir on 12.08.16.
 */
public interface MapPresenter extends Presenter<MapView>, FusedLocationProvider.OnLocationChangeListener{
    void updateData(double lat, double lon);
    void onDisableCameraMovement(boolean disable);
    void onMarkerClicked(Marker markerOptions);
    void sendLocation(double lat, double lon);

}
