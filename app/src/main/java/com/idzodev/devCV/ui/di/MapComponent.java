package com.idzodev.devCV.ui.di;

import com.idzodev.devCV.core.android.App;
import com.idzodev.devCV.core.di.scope.PerActivity;
import com.idzodev.devCV.domain.use_case.UserUseCase;
import com.idzodev.devCV.services.location.FusedLocationProvider;
import com.idzodev.devCV.ui.screens.map.MapActivity;
import com.idzodev.devCV.ui.screens.map.MapPresenter;
import com.idzodev.devCV.ui.screens.map.MapPresenterImpl;

import javax.inject.Singleton;

import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by vladimir on 12.08.16.
 */
@Subcomponent(modules = MapComponent.Module.class)
@PerActivity
public interface MapComponent {
    void inject(MapActivity mapActivity);

    @dagger.Module
    class Module{


        @Provides
        @PerActivity
        MapPresenter provideMapPresenter(App app, UserUseCase userUseCase){
            return new MapPresenterImpl(app, userUseCase);
        }

        @Singleton
        @PerActivity
        public FusedLocationProvider providerFusedLocationProvider(App app){
            return new FusedLocationProvider(app);
        }
    }
}
